var express = require('express');
var router = express.Router();
var controller = require('./video.controller.js');

router.get('/getVideo',controller.getVideo);

module.exports = router;
