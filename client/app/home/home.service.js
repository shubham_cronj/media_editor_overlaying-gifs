'use strict'

angular.module('mediaPlayer')
	.service('homeService',['$http',function($http) {
		// body...

		this.downloadVideo = function(body){
				return $http({
				  method: 'GET',
				  url: '/video/getVideo',
				  params: body
				}).then(function successCallback(response) {
				    return response.data;
				  }, function errorCallback(response) {
				    return response;
				  });
		}

		// return factory;
	}])
