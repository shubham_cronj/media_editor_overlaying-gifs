'use strict'

var fs = require('fs');
var youtubedl = require('youtube-dl');

var sqldb = require('../../sqldb');

var videoModel = sqldb.video;


module.exports.getVideo = function (req,res) {
	var link = req.query.link;

	// console.log(req.query,req.param,req.params);
	if(link)
	{

		videoModel.findOne({
			where : {
				link : link
			},
			attributes : ['file_path']
		}).then(function(data){
			console.log(data.file_path);		
			if(data){
				res.send({status:true, message: "already downloaded", filename: data.file_path});
			}
			else{
				var video = youtubedl(link);

				video.on('info', function(info) {
				  console.log('Download started');
				  console.log('filename: ' + info._filename);
				  console.log('size: ' + info.size);
				});
			 	
			 	var date = new Date();
			 	var a = date.getTime();
			 	var stringifiedTime = a.toString();
				video.pipe(fs.createWriteStream(__dirname+'/../../../client/uploads/video/'+'myVideo'+ stringifiedTime +'.mp4'));

				video.on('end', function complete(info) {
				  console.log('Finished Downloading');
				  videoModel.create({
					link : link,
					file_path : stringifiedTime
					}).then(function(data) {
						res.send({status:true, message: "downloaded successfully", filename: stringifiedTime});
					})	 
				});

				}
		})
		
	}

}