'use strict'

module.exports.port = process.env.PORT || 3000;

module.exports.sequelize = {

    uri: 'postgres://postgres:cronj@localhost:5432/media_player',
    options: {
      logging: false,
      dialect: 'postgres',
      define: {
        timestamps: false,
        underscored: true,
        freezeTableName: true
      }
    }
  }