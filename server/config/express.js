'use strict'

var express = require('express');

var bodyParser = require('body-parser');




module.exports = function(app){
	console.log("in middleware");
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({extended: false}));

	app.use(express.static(__dirname+'/../../client'));
	// app.use(express.static(__dirname+'/../uploads'));
}