'use strict'

var express = require('express');

var app = express();

var sqldb = require('./sqldb');

var config= require('./config/development.js');



require('./config/express.js')(app);
require('./routes.js')(app);




var startServer = function(){
	app.listen(3000, function(){
		console.log("server listening on 3000");
	});
}


sqldb.sequelize.sync().then(function() {
			startServer();
		return null;
	})
	.catch(function(err) {
		log('Server failed to start due to error: %s', err.stack);
	});