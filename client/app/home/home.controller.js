'use strict'

var gifPosLeft = {
	gifone : 0,
	giftwo : 0,
	gifthree : 0
}

var gifPosTop = {
	gifone : 0,
	giftwo : 0,
	gifthree : 0
}

var changePosLeft = {
	gifone : 0,
	giftwo : 0,
	gifthree : 0
}

var changePosTop = {
	gifone : 0,
	giftwo : 0,
	gifthree : 0
}

var resize = false;


angular.module('mediaPlayer',[])
	.controller('mediaPlayerCtrl',['$scope','homeService',function($scope, homeService) {
		// body...

		$scope.videoAvailable = false;

		$scope.showContent = false;

		$scope.toggle1 = false;
		$scope.toggle2 = false;
		$scope.toggle3 = false;


		$scope.toggleOne = function(){
				if($scope.toggle1)
				{
					$scope.toggle1 = false;
				}
				else{
					$scope.toggle1 = true;
				}
				gifPosLeft["gifone"] = 0;
				gifPosTop["gifone"] = 0;
			}

		$scope.toggleTwo = function(){
			if($scope.toggle2)
			{
				$scope.toggle2 = false;
			}
			else{
				$scope.toggle2 = true
			}
			gifPosLeft["giftwo"] = 0;
			gifPosTop["giftwo"] = 0;
		}

		$scope.toggleThree = function(){
			if($scope.toggle3)
			{
				$scope.toggle3 = false;
			}
			else{
				$scope.toggle3 = true
			}
			gifPosLeft["gifthree"] = 0;
			gifPosTop["gifthree"] = 0;
		}
		

		$scope.downloadVideo = function(){

			$scope.showContent = true;
			$scope.downloadLink = $scope.videoUrl;
			
			homeService.downloadVideo({link : $scope.videoUrl}).then(function(data){
				// console.log(data);
				if(data.status){
					$scope.videoAvailable = true;
					$scope.fileName = "myVideo"+data.filename+".mp4";
				}
				else{
					$scope.videoAvailable = false;
				}
			});
		}
	}])



	var temp = 0;
	var sX = 0;
	var sY = 0;
	var tX = 0;
	var tY = 0;
	var myFunctionOne = function(event,id){

		var player = document.getElementById("video");
		var element = document.getElementById(id);
		var playerHeight = player.clientHeight-4;
		var playerWidth = player.clientWidth-4;
		var imgHeight = element.clientHeight;
		var imgWidth = element.clientWidth;
		var maxTop = playerHeight - imgHeight;
		var maxLeft = playerWidth - imgWidth;

		if(resize){
			if(temp==0){
			temp++;
			sX = event.screenX;
			sY = event.screenY;
			// console.log("started",sX,sY);
			}
			else if(event.screenX != 0 && event.screenY !=0){
				tX = event.screenX;
				tY = event.screenY;
			}
			else{
				element.style.height = imgHeight + (tY - sY) + "px";
				element.style.width = imgWidth + (tX - sX) + 'px';
				temp = 0;
			}
		}
		else{

		if(temp==0){
			temp++;
			sX = event.screenX;
			sY = event.screenY;
			// console.log("started",sX,sY);
		}
		else if(event.screenX != 0 && event.screenY !=0){
			tX = event.screenX;
			tY = event.screenY;
			// console.log("moving",tX,tY);
		}
		else{

			changePosLeft[id] = tX - sX;
			changePosTop[id] = tY - sY;
			gifPosLeft[id] += changePosLeft[id];
			changePosLeft[id] = 0;
			gifPosTop[id] += changePosTop[id];
			changePosTop[id] = 0;
			if(gifPosLeft[id]<0){
				gifPosLeft[id] = 0;
				element.style.left = 0+"px";
			}
			else if(gifPosLeft[id] > maxLeft){
				gifPosLeft[id] = maxLeft;
				element.style.left = maxLeft+"px";
			}
			else{
				element.style.left = gifPosLeft[id]+"px";
			}
			
			if(gifPosTop[id] < 0){
				gifPosTop[id] = 0;
				element.style.top = 0+"px";
			}
			else if(gifPosTop[id] > maxTop){
				gifPosTop[id] = maxTop;
				element.style.top = maxTop+"px";
			}
			else{
				element.style.top = gifPosTop[id]+"px";
			}

			temp = 0;
		}
	}
	}

	var onkeydown = (function (ev) {
		if(ev.shiftKey){
			if(resize){
				resize = false;
			}
			else{
				resize = true;
			}
		};
		// console.log(resize);
	});



  // cursor styling
	var setCursorStyle = function(id){
		var element = document.getElementById(id);
				if(resize)
					element.style.cursor = "se-resize";
				else
					element.style.cursor = "move";
  };
  