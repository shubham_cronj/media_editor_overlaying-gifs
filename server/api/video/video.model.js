'use strict'

module.exports = function(sequelize,DataTypes){

	return sequelize.define('video',{

       id:{
       	type:DataTypes.INTEGER,
       	primaryKey:true,
       	autoIncrement: true

       },

       link:{
       	type:DataTypes.TEXT
       },

       file_path:{
       	type:DataTypes.TEXT
       }
	})
}
