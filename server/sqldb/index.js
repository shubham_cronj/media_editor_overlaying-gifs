'use strict'

var path = require('path');
var config = require('../config/development.js');
var Sequelize = require('sequelize');
var lodash = require('lodash');


var sequelize = new Sequelize(config.sequelize.uri, config.sequelize.options);


var db = {};


db["video"] = require(path.normalize(__dirname+'/../api/video/video.model.js'))(sequelize,Sequelize);



module.exports = lodash.extend({
  sequelize: sequelize,
  Sequelize: Sequelize
}, db);